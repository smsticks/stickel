from distutils.core import setup

with open("requirements.txt") as f:
    required = f.read().splitlines()

setup(
    name="stickel",
    version="0.1",
    description="Stickel's library of utilities",
    author="Steve Stickel",
    author_email="tdsticks@gmail.com",
    url="https://bitbucket.org/smsticks/stickel",
    license="GNU General Public License",
    long_description=open("README.md").read(),
    install_requires=required,
    # install_requires=[
    #     "paramiko==1.15.2",
    #     "pycrypto==2.6.1",
    #     "PyMySQL==0.6.7",
    #     "pysftp==0.2.9",
    #     "pytz==2016.10"
    # ],
    packages=["stickel"],
    # py_modules=["__init__","dateTime","db","emails","fileUtility","logger","percentage"],
)