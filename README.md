# Stickel
My library with various classes for everyday use

I recommend that you use virtual environment and pip to install needed libraries.  

- Run "pip install ." to install package

or 

- Run "pip install -e ." to install package